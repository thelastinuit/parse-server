FROM python:3.7.3-alpine3.9

WORKDIR /code

RUN \
    apk update && \
    apk --no-cache --update add \
    git nodejs nodejs-npm && \
    npm install npm -g --no-progress && \
    update-ca-certificates --fresh && \
    rm -rf /var/cache/apk/*

ENV PATH=./node_modules/.bin:$PATH \
    HOME=/code

RUN npm install -g parse-server

CMD ["parse-server", "--appId", "APPLICATION_ID", "--masterKey", "MASTER_KEY", "--databaseURI", "mongodb://localhost/test"]